import { expect } from "chai";
import { EngExp, EngExpError } from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("put description of test 1 here", () => {

        // put the body of the test here
        const e = new EngExp()
            .match("java")
            .maybe("script")
            .then(" is")
            .then(
                new EngExp()
                    .match(" not")
                    .optional()
            )
            .then(" awesome").asRegExp();

        const result = e.exec("javascript is awesome, javacoffee is awesome, but java is not awesome");

        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
        expect(e.test("java is awesome")).to.be.true;
        expect(e.test("javascript is awesome")).to.be.true;
        expect(e.test("java is not awesome")).to.be.true;
        expect(e.test("javascript is not awesome")).to.be.true;
        expect(e.test("javacoffee is awesome")).to.be.false;
        expect(result[0]).to.be.equal("javascript is awesome");
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.
    it("should throw an error if closing parentheses without opening it first", () => {
        expect(() => {
            new EngExp().endLevel();
        }).to.throw("Cannot end unbegun level");

        expect(() => {
            new EngExp().endCapture();
        }).to.throw("Cannot end unbegun capture");
    });

    it("should throw an error if try to call asRegExp() when parentheses are not completed", () => {
        expect(() => {
            new EngExp().beginCapture().asRegExp();
        }).to.throw("Incorrect amount of parentheses: (");

        expect(() => {
            new EngExp().beginLevel().asRegExp();
        }).to.throw("Incorrect amount of parentheses: (?:");
    });

    it("should throw an error if ending parenthesis does not match the begining parenthesis", () => {
        expect(() => {
            new EngExp().beginCapture().endLevel();
        }).to.throw("Cannot end capturing with level");

        expect(() => {
            new EngExp().beginLevel().endCapture();
        }).to.throw("Cannot end leveling with capture");
    });

    it("should or() with only current level", () => {
        const e = new EngExp()
            .match("out")
            .beginLevel()
            .then(" in1")
            .then(" in2")
            .or(
                new EngExp().match(" in3")
            )
            .endLevel()
            .asRegExp();
        expect(e.test("out in1 in2")).to.be.true;
        expect(e.test("out in3")).to.be.true;
        expect(e.test(" in3")).to.be.false;
    });

    it("should repeated() with only current level", () => {
        const e = new EngExp()
            .startOfLine()
            .then("out")
            .beginLevel()
            .then(" in1")
            .then(" in2")
            .repeated(1, 3)
            .endLevel()
            .endOfLine()
            .asRegExp();
        expect(e.test("out in1 in2")).to.be.true;
        expect(e.test("out in1 in2 in1 in2")).to.be.true;
        expect(e.test("out in1 in2 in1 in2 in1 in2")).to.be.true;
        expect(e.test("out in1 in2 in1 in2 in1 in2 in1 in2")).to.be.false;
        expect(e.test("out in1 in2out in1 in2")).to.be.false;
        expect(e.test("out in1 in2 in2")).to.be.false;
    });

    it("should optional() with only current level", () => {
        const e = new EngExp()
            .startOfLine()
            .then("out")
            .beginLevel()
            .then(" in1")
            .then(" in2")
            .optional()
            .endLevel()
            .endOfLine()
            .asRegExp();
        expect(e.test("out in1 in2")).to.be.true;
        expect(e.test("out")).to.be.true;
        expect(e.test("")).to.be.false;
        expect(e.test("out in1")).to.be.false;
    });

    it("should oneOrMore() with only current level", () => {
        const e = new EngExp()
            .startOfLine()
            .then("out")
            .beginLevel()
            .then(" in1")
            .then(" in2")
            .oneOrMore()
            .endLevel()
            .endOfLine()
            .asRegExp();
        expect(e.test("out in1 in2")).to.be.true;
        expect(e.test("out in1 in2 in1 in2")).to.be.true;
        expect(e.test("out")).to.be.false;
        expect(e.test("out in1 in2 in2")).to.be.false;
        expect(e.test("out in1 in2out in1 in2")).to.be.false;
    });

    it("should zeroOrMore() with only current level", () => {
        const e = new EngExp()
            .startOfLine()
            .then("out")
            .beginLevel()
            .then(" in1")
            .then(" in2")
            .zeroOrMore()
            .endLevel()
            .endOfLine()
            .asRegExp();
        expect(e.test("out in1 in2")).to.be.true;
        expect(e.test("out in1 in2 in1 in2")).to.be.true;
        expect(e.test("out")).to.be.true;
        expect(e.test("out in1 in2 in2")).to.be.false;
        expect(e.test("out in1 in2out in1 in2")).to.be.false;
        expect(e.test("")).to.be.false;
    });
});
