export class EngExpError extends Error { }

export class EngExp {
    private flags: string = "m";
    private pattern: string = "";
    // you can add new fields here
    private levelStart: number[] = [0];


    // don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // don't change these either
    public toString(): string {
        return this.asRegExp().source;
    }
    public valueOf(): string {
        return this.asRegExp().source;
    }
    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this to check for errors...
    public asRegExp(): RegExp {
        if (this.levelStart.length !== 1) {
            throw new EngExpError(`Incorrect amount of parentheses: ${this.pattern}`);
        }
        return new RegExp(this.pattern, this.flags);
    }

    // There is a bug somewhere in this code... can you find it?

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    public startOfLine(): EngExp {
        this.pattern += "^";
        return this;
    }

    public endOfLine(): EngExp {
        this.pattern += "$";
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.concatToInnerPattern("*");
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.concatToInnerPattern("+");
            return this;
        }
    }

    public optional(): EngExp {
        this.concatToInnerPattern("?");
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    public digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    public repeated(from: number, to?: number): EngExp {
        if (to === undefined) {
            this.concatToInnerPattern(`{${from}}`);
        } else {
            this.concatToInnerPattern(`{${from},${to}}`);
        }
        return this;
    }

    public multiple(pattern: string | EngExp, from: number, to?: number) {
        if (to === undefined) {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${from}}`;
        } else {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${from},${to}}`;
        }
        return this;
    }

    // you need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        this.concatToInnerPattern(`|(?:${EngExp.sanitize(pattern)})`);
        return this;
    }

    public beginLevel(): EngExp {
        this.pattern += "(?:";
        this.levelStart.push(this.pattern.length);
        return this;
    }

    public endLevel(): EngExp {
        if (this.levelStart.length === 1) {
            throw new EngExpError("Cannot end unbegun level");
        } else if (this.pattern.charAt(this.levelStart.pop() - 1) !== ':') {
            throw new EngExpError("Cannot end capturing with level");
        }

        this.pattern += ")";
        return this;
    }

    public beginCapture(): EngExp {
        this.pattern += "(";
        this.levelStart.push(this.pattern.length);
        return this;
    }

    public endCapture(): EngExp {
        if (this.levelStart.length === 1) {
            throw new EngExpError("Cannot end unbegun capture");
        } else if (this.pattern.charAt(this.levelStart.pop() - 1) !== '(') {
            throw new EngExpError("Cannot end leveling with capture");
        }

        this.pattern += ")";
        return this;
    }

    private concatToInnerPattern(pattern: string): void {
        const start: number = this.levelStart[this.levelStart.length - 1];
        this.pattern = `${this.pattern.substring(0, start)}(?:${this.pattern.substring(start)})${pattern}`;
    }
}

// ---- EXTRA CREDIT: named capture groups ----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}
interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more exampls of usage.
